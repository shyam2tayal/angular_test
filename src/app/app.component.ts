import { Component } from '@angular/core';
import { dataService } from './dataService.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
  providers: [dataService]
})
export class AppComponent {
  title = 'training';

  constructor(private data: dataService) {}

  ngOnInit() {
    this.title = this.data.getApp()
  }
}
