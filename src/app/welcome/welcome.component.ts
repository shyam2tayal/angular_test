import { Component, OnInit } from '@angular/core';
import { pipe, of, from, Observable, interval, fromEvent, catchError } from 'rxjs';
import {ajax} from 'rxjs/ajax';
import {filter, map} from 'rxjs/operators';

@Component({
  selector: 'app-welcome',
  templateUrl: './welcome.component.html',
  styleUrls: ['./welcome.component.scss']
})
export class WelcomeComponent implements OnInit {
  name = 'Shyam';
  actualName ='karan';
  data1 = ['apple', 'bannaa','orange' ,'grapes'];
  
  showNow = false;
  constructor() { }
  
  ngOnInit(): void {
    setTimeout(() => {
      this.showNow = true;
    }, 3000)
  }

}
